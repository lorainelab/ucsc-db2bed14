package org.lorainelab.tools.ucsc;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

@SpringBootApplication
public class Bed14GeneratorApplication implements CommandLineRunner {

    private static final Logger LOG = LoggerFactory.getLogger(Bed14GeneratorApplication.class);
    private static final String UCSC_QUERY = "select chrom,txStart,txEnd,name,score,strand,cdsStart,cdsEnd,exonCount,exonEnds,exonStarts,name2 from refGene";

    @Autowired
    JdbcTemplate jdbcTemplate;

    public static void main(String[] args) {
        SpringApplication.run(Bed14GeneratorApplication.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {
        File outputFile = new File("target/output.bed");
        com.google.common.io.Files.touch(outputFile);
        try (BufferedWriter bWriter = new BufferedWriter(new FileWriter(outputFile))) {
            jdbcTemplate.query(UCSC_QUERY, rs -> {
                try {
                    bWriter.write(
                            Joiner.on('\t').join(
                            rs.getString("chrom"),
                            rs.getString("txStart"),
                            rs.getString("txEnd"),
                            rs.getString("name"),
                            rs.getString("score"),
                            rs.getString("strand"),
                            rs.getString("cdsStart"),
                            rs.getString("cdsEnd"),
                            0,
                            rs.getString("exonCount"),
                            Joiner.on(',').join(calculateExonSizes(rs)),
                            Joiner.on(',').join(calculateExonRelativeStartPositions(rs)),
                            rs.getString("name2"),
                            "n/a"
                    ));
                    bWriter.newLine();
                } catch (IOException ex) {
                    LOG.error(ex.getMessage(), ex);
                }
            });
        }
    }

    private List<Integer> calculateExonSizes(ResultSet rs) throws SQLException {
        List<String> exonEnds = Splitter.on(',').trimResults().omitEmptyStrings().splitToList(rs.getString("exonEnds"));
        List<String> exonStarts = Splitter.on(',').trimResults().omitEmptyStrings().splitToList(rs.getString("exonStarts"));
        return IntStream.range(0, exonEnds.size()).mapToObj(idx -> {
            int end = Integer.parseInt(exonEnds.get(idx));
            int start = Integer.parseInt(exonStarts.get(idx));
            return end - start;
        }).collect(Collectors.toList());
    }

    private List<Integer> calculateExonRelativeStartPositions(ResultSet rs) throws SQLException {
        int annotationStart = Integer.parseInt(rs.getString("txStart"));
        List<String> exonStarts = Splitter.on(',').trimResults().omitEmptyStrings().splitToList(rs.getString("exonStarts"));
        return IntStream.range(0, exonStarts.size()).mapToObj(idx -> {
            return Integer.parseInt(exonStarts.get(idx)) - annotationStart;
        }).collect(Collectors.toList());
    }
}
